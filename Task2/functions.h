#pragma once

template<class T>
int compare(T val1, T val2) {
	if (val1 == val2) return 0;
	else if (val2 < val1) return -1;
	else return 1;
}

template<class T>
void swap(T& val1, T& val2) {
	T temp;
	temp = val1;
	val1 = val2;
	val2 = temp;
}

template<class T>
void bubbleSort(T arr[], int size) {
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (compare(arr[i], arr[j]) == -1) {
				swap(arr[i], arr[j]);
			}
		}
	}
}

template<class T>
void printArray(T arr[], int size) {
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}
